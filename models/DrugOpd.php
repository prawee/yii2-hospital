<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "drug_opd".
 *
 * @property string $HOSPCODE
 * @property string $PID
 * @property string $SEQ
 * @property string $DATE_SERV
 * @property string $CLINIC
 * @property string $DIDSTD
 * @property string $DNAME
 * @property integer $AMOUNT
 * @property string $UNIT
 * @property string $UNIT_PACKING
 * @property string $DRUGPRICE
 * @property string $DRUGCOST
 * @property string $PROVIDER
 * @property string $D_UPDATE
 */
class DrugOpd extends \yii\db\ActiveRecord
{
    public $NAME;
    public $CID;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'drug_opd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['HOSPCODE', 'PID', 'SEQ', 'DATE_SERV', 'CLINIC', 'DIDSTD', 'D_UPDATE'], 'required'],
            [['DATE_SERV', 'D_UPDATE','NAME','CID'], 'safe'],
            [['AMOUNT'], 'integer'],
            [['DRUGPRICE', 'DRUGCOST'], 'number'],
            [['HOSPCODE', 'CLINIC'], 'string', 'max' => 5],
            [['PID', 'PROVIDER'], 'string', 'max' => 15],
            [['SEQ'], 'string', 'max' => 16],
            [['DIDSTD'], 'string', 'max' => 24],
            [['DNAME'], 'string', 'max' => 255],
            [['UNIT'], 'string', 'max' => 3],
            [['UNIT_PACKING'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'HOSPCODE' => 'Hospcode',
            'PID' => 'Pid',
            'SEQ' => 'Seq',
            'DATE_SERV' => 'Date  Serv',
            'CLINIC' => 'Clinic',
            'DIDSTD' => 'Didstd',
            'DNAME' => 'Dname',
            'AMOUNT' => 'Amount',
            'UNIT' => 'Unit',
            'UNIT_PACKING' => 'Unit  Packing',
            'DRUGPRICE' => 'Drugprice',
            'DRUGCOST' => 'Drugcost',
            'PROVIDER' => 'Provider',
            'D_UPDATE' => 'D  Update',
            'NAME'=>'NAME'
        ];
    }

    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['HOSPCODE' => 'HOSPCODE','PID'=>'PID']);
    }
}
