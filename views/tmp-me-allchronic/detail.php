<?php
/**
 * @link http://www.yiiassets.com
 * 8/17/15 AD 9:59 PM
 * @copyright Copyright (c) 2015 infinitesteam
 * @author Prawee Wongsa <konkeanweb@gmai.com>
 * @license BSD-3-Clause
 */
use yii\grid\GridView;
use yii\helpers\VarDumper;


//VarDumper::dump($dataProvider,10,true);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'HOSPCODE',
        'NAME',
        'LNAME',
    ],
]);