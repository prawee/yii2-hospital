<?php
/*
 * 2015-01-22
 * @author Prawee Wongsa <prawee@hotmail.com>
 */
namespace app\components;

use Yii;
use yii\base\ActionFilter;
use yii\web\ForbiddenHttpException;

class AccessControl extends ActionFilter{
    public $params=[];
    public $denyCallback;
    private $separator = '.';

    public function beforeAction($action)
    {
        $user=Yii::$app->getUser();

        //module
        if($action->controller->module!= null){
            if($user->checkAccess($this->getItemName($action->controller->module).$this->separator.'*',$this->params)){
                //echo 'chkModule';
                return true;
            }
        }

        //controller
        if($user->checkAccess($this->getItemName($action->controller).$this->separator.'*',$this->params)){
            //echo 'chkController';
            return true;
        }

        //action
        if ($user->checkAccess($itemName = $this->getItemName($action), $this->params)) {
            //echo 'chkAction';
            return true;
        }

        if(isset($this->denyCallback)){
            if($user->isGuest){
                $user->loginRequired();
            }else{
                call_user_func($this->denyCallback, $itemName, $action);
            }
        }else{
            $this->denyAccess($user);
        }
        return false;
    }
    protected function denyAccess($user){
        if($user->isGuest){
            $user->loginRequired();
        }else{
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }
    private function getItemName($component) {
        return strtr($component->getUniqueId(), '/', $this->separator);
    }
}

