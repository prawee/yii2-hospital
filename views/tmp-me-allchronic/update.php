<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TmpMeAllchronic */

$this->title = 'Update Tmp Me Allchronic: ' . ' ' . $model->NAME;
$this->params['breadcrumbs'][] = ['label' => 'Tmp Me Allchronics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NAME, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tmp-me-allchronic-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
