<?php
/**
 * 15/8/2558 1:53 น.
 * @link http://www.yiiassets.com
 * @copyright Copyright (c) 2015 Prawee Wongsa <konkeanweb@gmail.com>
 * @license BSD-3-Clause
 * @author Prawee Wongsa <konkeanweb@gmail.com>
 */
namespace app\models;

use yii\base\Model;

class HtAgeForm extends Model{
    public $agestart=0;
    public $agestop=150;

    public function rules(){
        return [
            //[['agestart','agestop'], 'required','message'=>'กรอกข้อมูลให้ครบถ้วน'],

            ['agestart','required','message'=>'กรอกอายุเริ่ม'],
            ['agestart','integer','message'=>'ต้องเป็นตัวเลขเท่านั้น'],

            ['agestop','required','message'=>'กรอกอายุสิ้นสุด'],
            ['agestop','integer','message'=>'ต้องเป็นตัวเลขเท่านั้น'],

        ];
    }
}