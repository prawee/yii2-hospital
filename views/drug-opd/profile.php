<?php
/**
 * @link http://www.yiiassets.com
 * 8/17/15 AD 11:39 PM
 * @copyright Copyright (c) 2015 infinitesteam
 * @author Prawee Wongsa <konkeanweb@gmai.com>
 * @license BSD-3-Clause
 */
use yii\helpers\VarDumper;
use yii\grid\GridView;

//VarDumper::dump($dataProvider,10,true);


echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'HOSPCODE',
        'person.NAME',
        'PID',
        'SEQ',
        'DATE_SERV',
        'CLINIC',
        // 'DIDSTD',
        // 'DNAME',
        // 'AMOUNT',
        // 'UNIT',
        // 'UNIT_PACKING',
        // 'DRUGPRICE',
        // 'DRUGCOST',
        // 'PROVIDER',
        // 'D_UPDATE',

        ['class' => 'yii\grid\ActionColumn'],
    ],
]);
?>