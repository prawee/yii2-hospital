<?php
/**
 * 15/8/2558 1:36 น.
 * @link http://www.yiiassets.com
 * @copyright Copyright (c) 2015 Prawee Wongsa <konkeanweb@gmail.com>
 * @license BSD-3-Clause
 * @author Prawee Wongsa <konkeanweb@gmail.com>
 */


use yii\grid\GridView;
use yii\helpers\Html;

\yii\helpers\VarDumper::dump(Yii::$app->getSession()->get('hospcode'),10,true);

echo $this->render('_search',['model'=>$model]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'HOSPCODE',
        'nht',
        [
            'class'=>\yii\grid\ActionColumn::className(),
            'template'=>'{view}',
            'buttons'=>[
                'view'=>function($url,$model,$key){
                    if(Yii::$app->user->can('ht.index')){
                        $hospcode=Yii::$app->getSession()->get('hospcode');


                        $options =[
                            'title' => Yii::t('yii', 'View'),
                            'aria-label' => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                        ];
                        //echo $hospcode;
                        //\yii\helpers\VarDumper::dump($model,10,true);
                        if($hospcode == $model['HOSPCODE']){
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                        }else{
                            return 'ดูข้อมูลไม่ได้';
                        }

                    }
                    return null;
                }
            ]
        ],
    ],
]);
?>

