<?php

use yii\db\Schema;
use yii\db\Migration;

class m150815_053537_create_ht_permission extends Migration
{
    public function up()
    {
        $auth=Yii::$app->getAuthManager();

        $ht=$auth->createPermission('ht.index');
        $ht->description='HT index';
        $auth->add($ht);

    }

    public function down()
    {
        echo "m150815_053537_create_ht_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
