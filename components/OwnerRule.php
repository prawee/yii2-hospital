<?php
/*
 * 2015-01-22
 * @author Prawee Wongsa <prawee@hotmail.com>
 */
namespace app\components;

use yii\rbac\Rule;
class OwnerRule extends Rule{
    public $name='isOwner';
    public function execute($user, $item, $params)
    {
        return isset($params[key($params)])?$params[key($params)]->user_id==$user:false;
    }
}