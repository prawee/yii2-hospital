<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TmpMeAllchronic */

$this->title = 'Create Tmp Me Allchronic';
$this->params['breadcrumbs'][] = ['label' => 'Tmp Me Allchronics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tmp-me-allchronic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
