<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\DrugOpd;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DrugOpdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Drug Opds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drug-opd-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //Html::a('Create Drug Opd', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    $params= Yii::$app->getRequest()->queryParams;
    //\yii\helpers\VarDumper::dump($params,10,true);

    if(!empty($params['DrugOpdSearch']['NAME'])):
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'HOSPCODE',
            'person.NAME',
            //'PID',
            //'SEQ',
            'DATE_SERV',
            //'CLINIC',
            // 'DIDSTD',
             'DNAME',
            // 'AMOUNT',
            // 'UNIT',
            // 'UNIT_PACKING',
            // 'DRUGPRICE',
            // 'DRUGCOST',
            // 'PROVIDER',
            // 'D_UPDATE',
            [
                'label'=>'N',
                'format'=>'raw',
                'attribute'=>'AMOUNT',
                'value'=>function($data){
                    $params= Yii::$app->getRequest()->queryParams;
                    $name=$params['DrugOpdSearch']['NAME'];
                    $n=DrugOpd::find()->where([
                        'DATE_SERV'=>$data->DATE_SERV,
                        'HOSPCODE'=>$data->HOSPCODE,
                        'PID'=>$data->PID,
                    ])->count();
                    if($n>0){
                        return Html::a($n,['detail',
                            'HOSPCODE'=>$data->HOSPCODE,
                            'PID'=>$data->PID,
                            'DATE_SERV'=>$data->DATE_SERV,
                            'NAME'=>!empty($name)?$name:null,
                        ]);
                    }else{
                        return '0';
                    }
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php endif; ?>
</div>
