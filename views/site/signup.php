<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\VarDumper;
use app\models\Coffice;
use yii\helpers\ArrayHelper;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'id' => 'form-signup',
                'options' => [
                    'enctype' => 'multipart/form-data',
                ],
            ]); ?>

            <?php
            VarDumper::dump($model->getErrors(),10,true);
            VarDumper::dump($profile->getErrors(),10,true);
            ?>

            <?= $form->field($profile, 'photo')->fileInput() ?>

            <?php

            $listOffice=ArrayHelper::map(Coffice::find()->asArray()->all(),'hoscode','hosname');
            echo $form->field($profile, 'office_id')->dropDownList($listOffice,[
                'prompt'=>'==กรุณาเลือก=='
            ]);?>

            <?= $form->field($model, 'username') ?>

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($profile, 'firstname') ?>

            <?= $form->field($profile, 'lastname') ?>

            <div class="form-group">
                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
