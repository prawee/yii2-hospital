<?php
/*
 * 2015-01-22
 * @author Prawee Wongsa <prawee@hotmail.com>
 */
namespace app\components;

use yii\db\Expression;
class User extends \yii\web\User{
    public $identityClass='common\models\User';
    public $enableAutoLogin=true;
    public $loginUrl=['/site/login'];
    public $superAdmin=['admin'];
    public $fieldUpdateAfterLogin='updated_at';

    public function getIsSuperAdmin(){
        if($this->isGuest){
            return false;
        }
        return in_array($this->identity->username,$this->superAdmin);
    }
    public function checkAccess($operation,$params=[],$allowCaching=true){
        if($this->getIsSuperAdmin()){
            return true;
        }
        return parent::can($operation,$params,$allowCaching);
    }
    public function afterLogin($identity, $cookieBased, $duration)
    {
        $this->identity->setScenario(self::EVENT_AFTER_LOGIN);
        $this->identity->setAttribute($this->fieldUpdateAfterLogin,new Expression('now()'));
        $this->identity->save(false);
        return parent::afterLogin($identity, $cookieBased, $duration);
    }
}
