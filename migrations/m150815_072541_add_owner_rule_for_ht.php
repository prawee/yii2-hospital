<?php

use yii\db\Schema;
use yii\db\Migration;
use app\components\OwnerRule;

class m150815_072541_add_owner_rule_for_ht extends Migration
{
    public function up()
    {
        $auth=Yii::$app->getAuthManager();
        $rule=new OwnerRule();
        $auth->add($rule);


        $owner=$auth->createPermission('ht.index.owner');
        $owner->ruleName=$rule->name;
        $auth->add($owner);
    }

    public function down()
    {
        echo "m150815_072541_add_owner_rule_for_ht cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
