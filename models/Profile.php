<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $firstname
 * @property string $lastname
 * @property string $office_id
 * @property integer $usertype
 * @property string $photo
 * @property integer $count_login
 * @property string $last_login
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','photo','office_id'], 'required'],
            [['user_id', 'usertype', 'count_login'], 'integer'],
            [['last_login'], 'safe'],
            [['firstname', 'lastname', 'office_id', 'photo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'office_id' => 'Office ID',
            'usertype' => 'Usertype',
            'photo' => 'Photo',
            'count_login' => 'Count Login',
            'last_login' => 'Last Login',
        ];
    }
}
