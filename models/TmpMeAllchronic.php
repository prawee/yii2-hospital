<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tmp_me_allchronic".
 *
 * @property integer $ID
 * @property string $HOSPCODE
 * @property string $HOSPNAME
 * @property string $PID
 * @property string $CID
 * @property string $NAME
 * @property string $LNAME
 * @property string $BIRTH
 * @property string $SEX
 * @property string $TYPEAREA
 * @property string $DISCHARGE
 * @property string $DDISCHARGE
 * @property string $HOUSE
 * @property string $VILLAGE
 * @property string $VILLAGENAME
 * @property string $TAMBON
 * @property string $SUBDISTNAME
 * @property string $AMPUR
 * @property string $CHANGWAT
 * @property string $DM_DATE_DX
 * @property string $DM_DX
 * @property string $DM_TYPEDISCH
 * @property string $HT_DATE_DX
 * @property string $HT_DX
 * @property string $HT_TYPEDISCH
 * @property string $RENAL_DATE_DX
 * @property string $RENAL_DX
 * @property string $RENAL_TYPEDISCH
 * @property string $ISCHEMIC_DATE_DX
 * @property string $ISCHEMIC_DX
 * @property string $ISCHEMIC_TYPEDISCH
 * @property string $STROKE_DATE_DX
 * @property string $STROKE_DX
 * @property string $STROKE_TYPEDISCH
 * @property string $COPD_DATE_DX
 * @property string $COPD_DX
 * @property string $COPD_TYPEDISCH
 * @property string $ASTHMA_DATE_DX
 * @property string $ASTHMA_DX
 * @property string $ASTHMA_TYPEDISCH
 */
class TmpMeAllchronic extends \yii\db\ActiveRecord
{
    public $agestart;
    public $agestop;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tmp_me_allchronic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['BIRTH', 'DDISCHARGE'], 'safe'],
            [['HOSPCODE'], 'string', 'max' => 5],
            [['HOSPNAME', 'NAME', 'LNAME'], 'string', 'max' => 50],
            [['PID'], 'string', 'max' => 15],
            [['CID'], 'string', 'max' => 13],
            [['SEX', 'TYPEAREA', 'DISCHARGE'], 'string', 'max' => 1],
            [['HOUSE'], 'string', 'max' => 75],
            [['VILLAGE', 'TAMBON', 'AMPUR', 'CHANGWAT', 'DM_TYPEDISCH', 'HT_TYPEDISCH', 'RENAL_TYPEDISCH', 'ISCHEMIC_TYPEDISCH', 'STROKE_TYPEDISCH', 'COPD_TYPEDISCH', 'ASTHMA_TYPEDISCH'], 'string', 'max' => 2],
            [['VILLAGENAME', 'DM_DATE_DX', 'DM_DX', 'HT_DATE_DX', 'HT_DX', 'RENAL_DATE_DX',
                'RENAL_DX', 'ISCHEMIC_DATE_DX', 'ISCHEMIC_DX', 'STROKE_DATE_DX', 'STROKE_DX',
                'COPD_DATE_DX', 'COPD_DX', 'ASTHMA_DATE_DX', 'ASTHMA_DX'], 'string', 'max' => 255],
            [['SUBDISTNAME'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'HOSPCODE' => 'Hospcode',
            'HOSPNAME' => 'Hospname',
            'PID' => 'Pid',
            'CID' => 'Cid',
            'NAME' => 'Name',
            'LNAME' => 'Lname',
            'BIRTH' => 'Birth',
            'SEX' => 'Sex',
            'TYPEAREA' => 'Typearea',
            'DISCHARGE' => 'Discharge',
            'DDISCHARGE' => 'Ddischarge',
            'HOUSE' => 'House',
            'VILLAGE' => 'Village',
            'VILLAGENAME' => 'Villagename',
            'TAMBON' => 'Tambon',
            'SUBDISTNAME' => 'Subdistname',
            'AMPUR' => 'Ampur',
            'CHANGWAT' => 'Changwat',
            'DM_DATE_DX' => 'Dm  Date  Dx',
            'DM_DX' => 'Dm  Dx',
            'DM_TYPEDISCH' => 'Dm  Typedisch',
            'HT_DATE_DX' => 'Ht  Date  Dx',
            'HT_DX' => 'Ht  Dx',
            'HT_TYPEDISCH' => 'Ht  Typedisch',
            'RENAL_DATE_DX' => 'Renal  Date  Dx',
            'RENAL_DX' => 'Renal  Dx',
            'RENAL_TYPEDISCH' => 'Renal  Typedisch',
            'ISCHEMIC_DATE_DX' => 'Ischemic  Date  Dx',
            'ISCHEMIC_DX' => 'Ischemic  Dx',
            'ISCHEMIC_TYPEDISCH' => 'Ischemic  Typedisch',
            'STROKE_DATE_DX' => 'Stroke  Date  Dx',
            'STROKE_DX' => 'Stroke  Dx',
            'STROKE_TYPEDISCH' => 'Stroke  Typedisch',
            'COPD_DATE_DX' => 'Copd  Date  Dx',
            'COPD_DX' => 'Copd  Dx',
            'COPD_TYPEDISCH' => 'Copd  Typedisch',
            'ASTHMA_DATE_DX' => 'Asthma  Date  Dx',
            'ASTHMA_DX' => 'Asthma  Dx',
            'ASTHMA_TYPEDISCH' => 'Asthma  Typedisch',
        ];
    }
}
