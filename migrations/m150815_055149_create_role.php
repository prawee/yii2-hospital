<?php

use yii\db\Schema;
use yii\db\Migration;

class m150815_055149_create_role extends Migration
{
    public function up()
    {
        $auth=Yii::$app->getAuthManager();

        $admin=$auth->createRole('admin');
        $admin->description='Administrator';
        $auth->add($admin);

        $manager=$auth->createRole('manager');
        $manager->description='Manager';
        $auth->add($manager);

        $member=$auth->createRole('member');
        $member->description='Member';
        $auth->add($member);

    }

    public function down()
    {
        echo "m150815_055149_create_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
