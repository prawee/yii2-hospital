<?php
/**
 * 15/8/2558 1:31 น.
 * @link http://www.yiiassets.com
 * @copyright Copyright (c) 2015 Prawee Wongsa <konkeanweb@gmail.com>
 * @license BSD-3-Clause
 * @author Prawee Wongsa <konkeanweb@gmail.com>
 */

namespace app\controllers;

use app\components\AccessControl;
use app\models\HtAgeForm;
use Yii;
use yii\db\Exception;
use yii\helpers\VarDumper;
use yii\web\ConflictHttpException;
use yii\data\ArrayDataProvider;
use yii\web\Controller;

class HtController extends Controller
{
    public function behaviors(){
        return [
            'access'=>[
                'class'=>AccessControl::className(),
            ]
        ];
    }
    public function actionIndex()
    {
        $model=new HtAgeForm();
        $model->load(Yii::$app->request->post());

        if(!isset($model->agestart) and !isset($model->agestop)  || ($model->agestart > $model->agestop)){
            $model->agestart=0;
            $model->agestop=150;
        }

        $sql = "SELECT ht.HOSPCODE,ht.nht,pop.npop,ROUND((ht.nht/pop.npop*100),2) as rate
                FROM (
                  SELECT HOSPCODE,count(DISTINCT HOSPCODE,PID) as nht
                  FROM tmp_me_allchronic
                  WHERE
                    NOT ISNULL(HT_DX)
                    AND SUBSTR(HT_TYPEDISCH,1,2)='03'
                    AND TYPEAREA in ('1','3')
                    AND DISCHARGE='9'
                    AND TIMESTAMPDIFF(YEAR,BIRTH,CURDATE()) BETWEEN ".$model->agestart." AND ".$model->agestop." GROUP BY HOSPCODE ) as ht
                  LEFT JOIN (
                    SELECT HOSPCODE,count(DISTINCT HOSPCODE,PID) as npop
                    FROM person
                    WHERE TIMESTAMPDIFF(YEAR,BIRTH,CURDATE()) BETWEEN ".$model->agestart." AND ".$model->agestop." AND TYPEAREA in ('1','3')
                    AND DISCHARGE='9' GROUP BY HOSPCODE ) as pop ON ht.HOSPCODE=pop.HOSPCODE";

        try {
            $rawData = Yii::$app->db->createCommand($sql)->queryAll();
        } catch (Exception $e) {
            throw new ConflictHttpException('sql error');
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $rawData,
            'pagination' => FALSE,
        ]);
        return $this->render('index', [

            'dataProvider' => $dataProvider,
            'sql' => $sql,
            //'agestart' => $agestart,
            //'agestop' => $agestop,
            'model'=>$model,
        ]);
    }

    public function actionView(){

        $hospcode='07711';
        $sql = "SELECT CID,NAME,LNAME,BIRTH,SEX,TYPEAREA,HOUSE,VILLAGE,HT_DATE_DX,HT_DX
                FROM tmp_me_allchronic
                WHERE   HOSPCODE=".$hospcode."
                        AND TYPEAREA in ('1','3')
                        AND NOT ISNULL(HT_DX)
                        ORDER BY VILLAGE,HOUSE";

        try {
            $rawData = Yii::$app->db->createCommand($sql)->queryAll();
        } catch (Exception $e) {
            throw new ConflictHttpException('sql error');
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $rawData,
            'pagination' => FALSE,
        ]);
        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'sql' => $sql,
        ]);
    }
}