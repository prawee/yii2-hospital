<?php
/**
 * @link http://www.yiiassets.com
 * 8/18/15 AD 12:25 AM
 * @copyright Copyright (c) 2015 infinitesteam
 * @author Prawee Wongsa <konkeanweb@gmai.com>
 * @license BSD-3-Clause
 */
use yii\helpers\VarDumper;
use yii\grid\GridView;


$params=Yii::$app->request->queryParams;
//VarDumper::dump($params,10,true);

$this->title = 'Drug Opds';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = 'Detail of '.$params['NAME'];


//VarDumper::dump($person->attributes,10,true);

echo $person['BIRTH'];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'layout'=>'{items}',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        //'HOSPCODE',
        //'person.NAME',
        //'PID',
        //'SEQ',
        'DATE_SERV',
        //'CLINIC',
        // 'DIDSTD',
        'DNAME',
         'AMOUNT',
        // 'UNIT',
        // 'UNIT_PACKING',
        // 'DRUGPRICE',
        // 'DRUGCOST',
        // 'PROVIDER',
        // 'D_UPDATE',
    ],
]); ?>