<?php

namespace app\controllers;

use app\components\AccessControl;
use app\models\HtAgeForm;
use Yii;
use app\models\TmpMeAllchronic;
use app\models\TmpMeAllchronicSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;

/**
 * TmpMeAllchronicController implements the CRUD actions for TmpMeAllchronic model.
 */
class TmpMeAllchronicController extends Controller
{
    public function behaviors()
    {
        return [
            'access'=>[
                'class'=>AccessControl::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TmpMeAllchronic models.
     * @return mixed
     */
    /*public function actionIndex()
    {
        $searchModel = new TmpMeAllchronicSearch();


        $params=Yii::$app->request->queryParams;
        if(empty($params)){
            $params=[
                'TmpMeAllchronicSearch'=>[
                    'agestart'=>0,
                    'agestop'=>150,
                ]
            ];
        }


        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }*/
    public function actionIndex(){
        $model=new HtAgeForm();
        $model->load(Yii::$app->request->post());

        if(!isset($model->agestart) and !isset($model->agestop)  || ($model->agestart > $model->agestop)){
            $model->agestart=0;
            $model->agestop=150;
        }

        $sql = "SELECT ht.HOSPCODE,ht.nht,pop.npop,ROUND((ht.nht/pop.npop*100),2) as rate
                FROM (
                  SELECT HOSPCODE,count(DISTINCT HOSPCODE,PID) as nht
                  FROM tmp_me_allchronic
                  WHERE
                    NOT ISNULL(HT_DX)
                    AND SUBSTR(HT_TYPEDISCH,1,2)='03'
                    AND TYPEAREA in ('1','3')
                    AND DISCHARGE='9'
                    AND TIMESTAMPDIFF(YEAR,BIRTH,CURDATE()) BETWEEN ".$model->agestart." AND ".$model->agestop." GROUP BY HOSPCODE ) as ht
                  LEFT JOIN (
                    SELECT HOSPCODE,count(DISTINCT HOSPCODE,PID) as npop
                    FROM person
                    WHERE TIMESTAMPDIFF(YEAR,BIRTH,CURDATE()) BETWEEN ".$model->agestart." AND ".$model->agestop." AND TYPEAREA in ('1','3')
                    AND DISCHARGE='9' GROUP BY HOSPCODE ) as pop ON ht.HOSPCODE=pop.HOSPCODE";

        try {
            $rawData = Yii::$app->db->createCommand($sql)->queryAll();
        } catch (Exception $e) {
            throw new ConflictHttpException('sql error');
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $rawData,
            'pagination' => FALSE,
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single TmpMeAllchronic model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TmpMeAllchronic model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TmpMeAllchronic();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TmpMeAllchronic model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TmpMeAllchronic model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TmpMeAllchronic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TmpMeAllchronic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TmpMeAllchronic::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionDetail(){
        $hospcode=Yii::$app->getRequest()->get('hospcode');
        $npop=Yii::$app->getRequest()->get('npop');

        $searchModel = new TmpMeAllchronicSearch();
        $params=Yii::$app->request->queryParams;
        if(!empty($hospcode) && !empty($npop)){
            $params['TmpMeAllchronicSearch']['HOSPCODE']=$hospcode;
            //$params['TmpMeAllchronicSearch']['npop']=$npop;
        }
        VarDumper::dump($params);
        $dataProvider = $searchModel->search($params);
        return $this->render('detail', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
