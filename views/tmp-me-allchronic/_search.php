<?php
/**
 * 15/8/2558 1:59 น.
 * @link http://www.yiiassets.com
 * @copyright Copyright (c) 2015 Prawee Wongsa <konkeanweb@gmail.com>
 * @license BSD-3-Clause
 * @author Prawee Wongsa <konkeanweb@gmail.com>
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="row">
    <?php $form=ActiveForm::begin()?>
    <div class="col-md-4">
        <?=$form->field($model,'agestart')?>
    </div>
    <div class="col-md-4">
        <?=$form->field($model,'agestop')?>
    </div>
    <div class="col-md-4">
        <div style="margin-top:25px;">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
        </div>
    </div>
    <?php ActiveForm::end()?>
</div>
